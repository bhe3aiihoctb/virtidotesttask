﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ControlTest
{
	[TemplatePart(Name = "PART_ItemsPresenter", Type = typeof(ItemsPresenter))]
	[TemplatePart(Name = "PART_CountButton", Type = typeof(ButtonBase))]
	public partial class UniformItemsControl : ItemsControl
	{
		private const string TEMPLATE_ITEMS_PRESENTER_NAME = "PART_ItemsPresenter";
		private const string TEMPLATE_COUNT_BUTTON_NAME = "PART_CountButton";

		private ButtonBase countButton;
		private Panel itemsPanel;
		private ICollectionView toolTipCollectionView;
		private ItemsControl toolTipItemsControl;

		private readonly HashSet<object> visibleContent = new HashSet<object>();

		public ItemsPanelTemplate ToolTipItemsPanel {
			get { return (ItemsPanelTemplate)GetValue(ToolTipItemsPanelProperty); }
			set { SetValue(ToolTipItemsPanelProperty, value); }
		}
		public static readonly DependencyProperty ToolTipItemsPanelProperty =
			DependencyProperty.Register(nameof(ToolTipItemsPanel), typeof(ItemsPanelTemplate), typeof(UniformItemsControl), new PropertyMetadata(GetDefaultItemsPanelTemplate()));

		public DataTemplate ToolTipItemTemplate {
			get { return (DataTemplate)GetValue(ToolTipItemTemplateProperty); }
			set { SetValue(ToolTipItemTemplateProperty, value); }
		}
		public static readonly DependencyProperty ToolTipItemTemplateProperty =
			DependencyProperty.Register(nameof(ToolTipItemTemplate), typeof(DataTemplate), typeof(UniformItemsControl), new PropertyMetadata((DataTemplate)null));


		public ControlTemplate ToolTipTemplate {
			get { return (ControlTemplate)GetValue(ToolTipTemplateProperty); }
			set { SetValue(ToolTipTemplateProperty, value); }
		}
		public static readonly DependencyProperty ToolTipTemplateProperty =
			DependencyProperty.Register(nameof(ToolTipTemplate), typeof(ControlTemplate), typeof(UniformItemsControl), new PropertyMetadata());

		public ControlTemplate CountButtonTemplate {
			get { return (ControlTemplate)GetValue(CountButtonTemplateProperty); }
			set { SetValue(CountButtonTemplateProperty, value); }
		}
		public static readonly DependencyProperty CountButtonTemplateProperty =
			DependencyProperty.Register(nameof(CountButtonTemplate), typeof(ControlTemplate), typeof(UniformItemsControl), new PropertyMetadata());

		public int TakeElements {
			get { return (int)GetValue(TakeElementsProperty); }
			set { SetValue(TakeElementsProperty, value); }
		}
		public static readonly DependencyProperty TakeElementsProperty =
			DependencyProperty.Register(nameof(TakeElements), typeof(int), typeof(UniformItemsControl), new PropertyMetadata(0));


		public UniformItemsControl()
		{
			InitializeComponent();
		}

		private void OnLoaded(object sender, RoutedEventArgs e)
		{
			InitializeOnLoadComponents();
			UpdateControlData();
		}

		private void InitializeOnLoadComponents()
		{
			countButton = (ButtonBase)GetTemplateChild(TEMPLATE_COUNT_BUTTON_NAME);
			countButton.Visibility = Visibility.Collapsed;
			countButton.Template = CountButtonTemplate;

			var itemsPresenter = GetTemplateChild(TEMPLATE_ITEMS_PRESENTER_NAME);
			itemsPanel = (Panel)VisualTreeHelper.GetChild(itemsPresenter, 0);

			toolTipCollectionView = new CollectionViewSource() { Source = ItemsSource }.View;
			toolTipCollectionView.Filter = new Predicate<object>(FilterVisibleContent);

			var toolTip = new ToolTip();
			toolTip.Template = ToolTipTemplate;
			toolTipItemsControl = new ItemsControl();
			toolTipItemsControl.ItemTemplate = ToolTipItemTemplate;
			toolTipItemsControl.ItemsPanel = ToolTipItemsPanel;
			toolTipItemsControl.ItemsSource = toolTipCollectionView;
			toolTip.Content = toolTipItemsControl;
			countButton.ToolTip = toolTip;
		}

		private bool FilterVisibleContent(object content)
		{
			return !visibleContent.Contains(content);
		}

		protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
		{
			base.OnRenderSizeChanged(sizeInfo);
			UpdateControlData();
		}

		protected override void OnItemsChanged(NotifyCollectionChangedEventArgs e)
		{
			base.OnItemsChanged(e);
			UpdateControlData();
		}

		/// <summary>
		/// You can invoke it any time more if something changed, e.g. some templates in style or items size
		/// </summary>
		private void UpdateControlData()
		{
			if (countButton == null) { return; }
			if (itemsPanel == null) { return; }

			var hiddenCount = 0;
			var visibleItemsChanged = false;
			for (var i = 0; i < itemsPanel.Children.Count; i++)
			{
				if (!(itemsPanel.Children[i] is ContentPresenter child))
				{
					continue;
				}
				var alreadyInvisivble = child.Visibility == Visibility.Collapsed || child.DesiredSize == default(Size);
				var outOfBounds = !alreadyInvisivble && ((i >= TakeElements) || IsChildOutOfBounds(itemsPanel, child));

				if (!child.IsLoaded)
				{
					child.Loaded += OnChildLoaded;
				}

				child.Opacity = outOfBounds ? 0 : 1;

				if (outOfBounds)
				{
					hiddenCount++;
				}
				visibleItemsChanged |= UpdateVisibleContent(child, !(outOfBounds || alreadyInvisivble));
			}
			countButton.Content = hiddenCount;
			countButton.Visibility = hiddenCount > 0 ? Visibility.Visible : Visibility.Collapsed;
			if (visibleItemsChanged)
			{
				toolTipCollectionView.Refresh();
				toolTipItemsControl.Items.Refresh();
			}
		}

		private void OnChildLoaded(object sender, RoutedEventArgs e)
		{
			if(sender is ContentPresenter child)
			{
				child.Loaded -= OnChildLoaded;
			}
			UpdateControlData();
		}

		private bool UpdateVisibleContent(ContentPresenter contentPresenter, bool visible)
		{
			if(visible && visibleContent.Add(contentPresenter.Content))
			{
				return true;
			}
			if(!visible && visibleContent.Remove(contentPresenter.Content))
			{
				return true;
			}
			return false;
		}

		private static bool IsChildOutOfBounds(UIElement parent, UIElement child)
		{
			var bottomRightPoint = VisualTreeHelper.GetOffset(child) + new Vector(child.DesiredSize.Width, child.DesiredSize.Height);
			return bottomRightPoint.X > parent.DesiredSize.Width || bottomRightPoint.Y > parent.DesiredSize.Height;
		}

		private static ItemsPanelTemplate GetDefaultItemsPanelTemplate()
		{
			ItemsPanelTemplate template = new ItemsPanelTemplate(new FrameworkElementFactory(typeof(StackPanel)));
			template.Seal();
			return template;
		}
	}
}
