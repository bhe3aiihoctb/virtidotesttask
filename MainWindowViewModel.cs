﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ControlTest
{
	public class MainWindowViewModel : ViewModel
	{
		public ObservableCollection<string> SomeItems { get; set; }
		public ObservableCollection<string> SomeItemsWebElements { get; set; }

		public MainWindowViewModel()
		{
			SomeItems = new ObservableCollection<string>(Enumerable.Range(1, 10).Select(x => $"Element {x}"));
			SomeItemsWebElements = new ObservableCollection<string>();
			FillWebItems(SomeItemsWebElements);
		}

		private async void FillWebItems(ObservableCollection<string> collection)
		{
			using (var httpClient = new HttpClient())
			{
				try
				{
					var response = await GetWebResponseAsync(httpClient);
					ParseAndFill(collection, response);
				}
				catch
				{

				}
			}
		}

		private async Task<string> GetWebResponseAsync(HttpClient httpClient)
		{
			var response = await httpClient.GetStringAsync("https://bash.im");
			return response;
		}

		private void ParseAndFill(ObservableCollection<string> collection, string response)
		{
			var matches = Regex.Matches(response, "<div class=.quote__body.>\n +(.*)\n");
			var maxCount = Math.Min(matches.Count, 10);
			for (int i = 1; i < maxCount; i++)
			{
				collection.Add(matches[i].Groups[1].Value.Replace("<br>", Environment.NewLine));
			}
		}

	}
}
